package ru.tsc.marfin.tm.exception.system;

import ru.tsc.marfin.tm.exception.AbstractException;

public class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(String argument) {
        super("Error! Argument `" + argument + "` not supported...");
    }

}
