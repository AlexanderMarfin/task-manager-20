package ru.tsc.marfin.tm.command.system;

import ru.tsc.marfin.tm.api.service.ICommandService;
import ru.tsc.marfin.tm.command.AbstractCommand;
import ru.tsc.marfin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }
}
