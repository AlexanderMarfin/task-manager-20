package ru.tsc.marfin.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldException{

    public ProjectIdEmptyException() {
        super("Error! Project Id is empty...");
    }

}
