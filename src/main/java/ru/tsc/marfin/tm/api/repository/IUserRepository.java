package ru.tsc.marfin.tm.api.repository;

import ru.tsc.marfin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    User removeByLogin(String login);

}
